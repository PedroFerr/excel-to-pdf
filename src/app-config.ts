import * as Types from './app-interfaces';


export const CONFIG: Types.AppConfig = {
    path: {
        excelTemplate: './public/_templates/template.xlsx'
        , excelBufferCopy: './public/z-buffer-bin/new.xlsx'
        , pdfResult: './public/output/file.pdf'
    }
};