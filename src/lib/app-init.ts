// ===================================================== //
// Load environment variables - things like passwords and sensitive information - into '.env' file
// MIND YOU:
//     1) to add them to '.gitignore' to NOT go to the repository, in order to not be seen at the code level.
//     2) Npm dependency 'dotenv' should be installed.
//     3) should be the very 1st require/import of all, obviously.
require('dotenv').config();
// ===================================================== //
// 'env.example' - fake clown of this 'env' files, to send to repo, just to show to co-workers what variables are hidden and in which format.
// ===================================================== //

import express from 'express';
import * as Types from '../app-interfaces';

export default class AppBootstrap {

    server: express.Application;
    serverPort: number;

    constructor(portN: number | null = null) {
        this.server = express();
        this.serverPort = portN || 6000;
    }

    start(callback: (returnedObj: Types.ServerJS) => void) {
        // Start the server, and send it back:
        const liveServer = this.server.listen(
            this.serverPort, () => {
                console.log(`A "Node + Express + TypeScript" server is running @ port ${this.serverPort}!\n.\n.\nLet the games begin!\n.\n.`);
            }
        );

        callback(
            {
                node: { appFramwork: this.server, server: liveServer }
            }
        );
    }

}