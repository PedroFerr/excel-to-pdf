// Get your/new API KEY @ https://www.convertapi.com/a (see 'env.example', for details)
var convertapi = require('convertapi')(process.env.CONVERTAPI_KEY);

import Utils from './app-utils';
const appUtils = new Utils;

export default class PdfMaker {

    constructor() {}

    printIt (fromFileDir: string, toPDFdir: string) {

        convertapi.convert('pdf', { File: fromFileDir })
            .then((result: any) => {
                appUtils.logIt({ body: { text: `PDF creation SUCCESS!`, colorText: 'cyan'} });
                // Get converted file at some WWWW url
                appUtils.logIt({ body: { text: `Download it @ ${result.file.url}`, colorText: 'yellow'} });
                // Save to file
                return result.file.save(toPDFdir);
            })
            .then((file: any) => {
                appUtils.logIt({ body: { text: `File saved @ "${file}"`, colorText: 'bgMagenta'} });

            })
        ;

    }
}