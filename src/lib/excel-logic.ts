import * as Types from '../app-interfaces';

import Utils from './app-utils';
const appUtils = new Utils;

export default class ExcelLogic {
    
    constructor(
        private workBook: any
    ) { }

    handleIt(origExcelDir: string, resultToDir: string, callback: (escelIsManipulated: boolean) => void) {

        this.workBook.xlsx.readFile(origExcelDir)
            .then(() => {
                this.manipulateData( (rowManipulated: any) => {
                    // Commit the changes made out of the Template's manipulation:
                    rowManipulated.commit();
                    // Save them into a 'resultToDir' DIFFERENT excel file:
                    return this.workBook.xlsx.writeFile(resultToDir);
                })
            })
            // Inform the world and call back, so PDF can, now, be created:
            .then(() => {
                const newConsoleParams: Types.ConsoleBeauty = {
                    body: { text: `New manipulated Excel saved @ "${resultToDir}"`, colorText: 'magenta'}
                };
                appUtils.logIt(newConsoleParams);
                
                callback(true);
            })
        ;
    }

    private manipulateData( callback: (manipulatedRow: any) => void) {

        const worksheet = this.workBook.getWorksheet(1);
        const row = worksheet.getRow(4);
        // Write $C4 cell value (check API and examples @ https://github.com/exceljs/exceljs - you can even format cells or add images over a range of cells)
        row.getCell(3).value = 'This is a NEW MongoDB var value!';

        callback(row);
    }
}