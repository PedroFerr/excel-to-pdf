import withColor from 'colors/safe';

import * as Types from '../app-interfaces';

export default class Utils {

    logIt(formatObj: Types.ConsoleBeauty): void {
        // If not all are passed in, use the default props, for 'header' and 'body', covering ALL props.
        const thisFormat = {
            header: Object.assign({}, Types.ConsoleBeautyDefault.header , formatObj.header)
            , body: Object.assign({}, Types.ConsoleBeautyDefault.body, formatObj.body)
        };

        // 'header' property is Typed as non compulsory; might not come - watch the Type's possibilities!
        const header = thisFormat.header ? thisFormat.header : null;
        const headerText = header ? header.text : '';
        // Colors, used as indexes for 'colors/safe' can not be 'undefined' or 'null'
        const headerClrLmt = header ? header.colorLimit ?
                header.colorLimit
                :
                'white'
            :
            'white'
        ;
        const headerClrTxt = header ? header.colorText ?
                header.colorText
                :
                'white'
            :
            'white'
        ;

        // 'body' always comes - is Typed as non compulsory:
        const body = thisFormat.body;
        const bodyClrTxt = body.colorText || 'white'

        // -------------------------

        if (formatObj.header) {
            console.log(withColor[headerClrLmt](`==================================`));
            console.log(withColor[headerClrTxt](headerText || ''));
            console.log(withColor[headerClrLmt](`==================================`));
        }

        if (formatObj.body.text) { console.log(withColor[bodyClrTxt](thisFormat.body.text || '')); }
        if (formatObj.body.data) { console.log(withColor[bodyClrTxt](thisFormat.body.data)); }

        if (formatObj.header) {
            console.log(withColor[headerClrLmt](`==================================`));    
        }
    }
}