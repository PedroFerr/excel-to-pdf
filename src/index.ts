import Excel from 'exceljs';
const workbook = new Excel.Workbook();

import { CONFIG } from './app-config';
import * as Types from './app-interfaces';

// Finally, the App's library Classes/Components:
import Utils from './lib/app-utils';
const appUtils = new Utils;

import AppBootstrap from './lib/app-init';
const bootstrap = new AppBootstrap(6001);

import ExcelLogic from './lib/excel-logic';
const excelLogic = new ExcelLogic(workbook);

import PdfMaker from './lib/pdf-maker';
const pdfMaker = new PdfMaker;

bootstrap.start( (data: Types.ServerJS) => {

    const newConsoleParams: Types.ConsoleBeauty = {
        header: { text: 'App "excel-to-pdf" bootstrapped with   \nthe current Express framework settings:' }
        , body: { data: data.node.appFramwork.settings, colorText: 'green' }
    };
    appUtils.logIt(newConsoleParams);

    const
        oldF = CONFIG.path.excelTemplate
        , newF = CONFIG.path.excelBufferCopy

    ;
    excelLogic.handleIt(oldF, newF , (isExcelManipulated: boolean) => {

        if(isExcelManipulated) {
            appUtils.logIt({ body: { text: `Please wait... PDF creation in course...`}});

            // Give it time for the 'fs' to write the file, on calling back to here, before printing the PDF
            setTimeout( () => pdfMaker.printIt(CONFIG.path.excelBufferCopy, CONFIG.path.pdfResult), 1000);
        }
    });
})

