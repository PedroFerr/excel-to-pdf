export interface ServerJS  {
    node: {
        appFramwork: any;
        server: any;
    }
}

export interface AppConfig {
    path: {
        excelTemplate: string
        , excelBufferCopy: string
        , pdfResult: string
    }
}

// To use colors on console.log, through "import * from 'colors/safe';"
// Check it @ https://www.npmjs.com/package/colors
export interface ConsoleBeauty {
    header?: {
        colorLimit?: ConsoleBeautyColors;
        colorText?: ConsoleBeautyColors;
        text?: string;
        data?: any | null;
    }
    , body: {
        colorText?: ConsoleBeautyColors
        , text?: string
        , data?: any | null
    }
}
// ... there's more color possibilities; for now this is enough.
type ConsoleBeautyColors =
    | 'black'
    | 'red'
    | 'green'
    | 'yellow'
    | 'blue'
    | 'magenta'
    | 'cyan'
    | 'white'
    | 'gray'
    | 'grey'

    | 'bgBlack'
    | 'bgRed'
    | 'bgGreen'
    | 'bgYellow'
    | 'bgBlue'
    | 'bgMagenta'
    | 'bgCyan'
    | 'bgWhite'
;
export const ConsoleBeautyDefault: ConsoleBeauty = {
    header: {
        colorLimit: 'red'
        , colorText: 'cyan'
        , text: ''
        , data: null
    }
    , body: {
        colorText: 'white'
        , text: ''
        , data: null
    }
}